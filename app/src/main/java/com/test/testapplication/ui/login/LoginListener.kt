package com.test.testapplication.ui.login


interface LoginListener {
    fun onStarted()
    fun onSuccess(username: String)
    fun onFailure(message: String)
}