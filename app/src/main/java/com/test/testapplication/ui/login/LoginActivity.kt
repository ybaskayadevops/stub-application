package com.test.testapplication.ui.login.viewmodel


import android.content.Intent
import android.os.Bundle
import com.test.testapplication.R
import com.test.testapplication.data.preferences.PreferenceProvider
import com.test.testapplication.databinding.ActivityLoginBinding
import com.test.testapplication.ui.base.BaseActivity
import com.test.testapplication.ui.login.LoginListener
import com.test.testapplication.ui.login.LoginViewModel
import com.test.testapplication.ui.overview.OverviewActivity
import com.test.testapplication.util.CustomDialog


class LoginActivity : BaseActivity<ActivityLoginBinding, LoginViewModel>(), LoginListener {


    override fun onCreate(savedInstanceState: Bundle?) {
        setTitle();
        super.onCreate(savedInstanceState)
        mViewModel = LoginViewModel(this)
        mViewModel.loginListener = this
        mBinding.viewModel = mViewModel

        if (PreferenceProvider(this).getUserName() != null) {
            val intent = Intent(this, OverviewActivity::class.java)
            startActivity(intent)
        }
    }

    private fun setTitle() {
        setTitle(resources.getString(R.string.logon));
    }

    override fun getLayoutRes(): Int {
        return R.layout.activity_login
    }

    override fun onStarted() {

    }

    override fun onSuccess(username: String) {
        PreferenceProvider(this).saveUserName(username)
        val intent = Intent(this, OverviewActivity::class.java)
        startActivity(intent)

        finish()
    }

    override fun onFailure(message: String) {

        CustomDialog().showAlertDialogButton(
            this, resources.getString(R.string.warning),
            message, resources.getString(R.string.OK)
        )
    }

}
