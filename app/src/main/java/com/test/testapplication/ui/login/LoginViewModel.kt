package com.test.testapplication.ui.login

import android.content.Context
import android.content.Intent
import android.text.TextUtils
import android.view.View
import com.test.testapplication.R
import com.test.testapplication.ui.base.BaseViewModel
import com.test.testapplication.ui.login.viewmodel.LoginActivity

class LoginViewModel(val context: Context) : BaseViewModel() {

    var username: String? = null
    var password: String? = null

    var loginListener: LoginListener? = null


    fun onLoginButtonClick(view: View) {
        loginListener?.onStarted()
        if (!LoginValidator().validateUserName(username)
            || !LoginValidator().validatePassword( password) ) {

            loginListener?.onFailure(context.resources.getString(R.string.all_fields_are_mandatory))
            return
        } else {
            loginListener?.onSuccess(username!!);
        }

    }

    fun onLogin(view: View) {


        Intent(view.context, LoginActivity::class.java).also {
            view.context.startActivity(it)
        }
    }
}