package com.test.testapplication.ui.overview

import android.content.Intent
import android.os.Bundle
import android.support.design.widget.Snackbar
import com.test.testapplication.R
import com.test.testapplication.data.preferences.PreferenceProvider
import com.test.testapplication.databinding.ActivityOverviewBinding
import com.test.testapplication.ui.base.BaseActivity
import com.test.testapplication.ui.login.viewmodel.LoginActivity

class OverviewActivity : BaseActivity<ActivityOverviewBinding, OverviewViewModel>(),
    OverviewListener {


    val drawables: IntArray = intArrayOf(
        R.drawable.ic_local_bar_black_24dp,
        R.drawable.ic_local_car_wash_black_24dp, R.drawable.ic_local_dining_black_24dp,
        R.drawable.ic_local_gas_station_black_24dp
    )
    val ADAPTER_ITEM_COUNT = 20;

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        mViewModel = OverviewViewModel()
        mBinding.viewModel = mViewModel
        mViewModel.overviewListener = this

        createData();
    }

    fun createData() {
        var rnd = (0..(drawables.size - 1)).random()

        for (i in 1 until ADAPTER_ITEM_COUNT) {
            mViewModel.addRow(drawables[rnd], "Label " + i, "Sublabel " + i)
        }
    }

    override fun onStart() {
        super.onStart()
        val snack = Snackbar.make(
            mBinding.rootView,
            PreferenceProvider(this).getUserName().toString(), Snackbar.LENGTH_LONG
        )
        snack.show()
    }

    override fun getLayoutRes(): Int {
        return R.layout.activity_overview
    }

    override fun onBackPressed() {

        PreferenceProvider(this).clearUserName()
        val intent = Intent(this, LoginActivity::class.java)
        startActivity(intent)
        finish()
    }
}
