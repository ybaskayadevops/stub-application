package com.test.testapplication.ui.overview

import android.databinding.DataBindingUtil
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.ViewGroup
import com.test.testapplication.R
import com.test.testapplication.databinding.ItemOverviewBinding
import com.test.testapplication.model.Overview

class OverviewAdapter : RecyclerView.Adapter<OverviewAdapter.ViewHolder>() {
    private val Overviews: MutableList<Overview> = mutableListOf()

    override fun onCreateViewHolder(parent: ViewGroup, position: Int): ViewHolder {
        val binding: ItemOverviewBinding = DataBindingUtil.inflate(
            LayoutInflater.from(parent.context),
            R.layout.item_overview, parent, false
        )
        return ViewHolder(binding, this)
    }

    override fun getItemCount(): Int {
        return Overviews.size
    }

    override fun onBindViewHolder(viewHolder: ViewHolder, position: Int) {

        if (!Overviews[position].title.isEmpty()) {
            viewHolder.binding.textviewTitle.setText(Overviews[position].title)
        }
        if (!Overviews[position].subTitle.isEmpty()) {
            viewHolder.binding.textviewSubTitle.setText(Overviews[position].subTitle)
        }

        viewHolder.binding.imageViewOverview.setImageResource(Overviews[position].avatarResId)


    }

    fun addItem(overview: Overview) {
        Overviews.add(overview)
        notifyDataSetChanged()
    }

    fun getItems(): MutableList<Overview> {
        return Overviews
    }

    fun removeItem(position: Int) {
        Overviews.removeAt(position)
        notifyDataSetChanged()
    }

    class ViewHolder(binding: ItemOverviewBinding, adapter: OverviewAdapter) :
        RecyclerView.ViewHolder(binding.root) {
        val binding: ItemOverviewBinding = binding
        val adapter: OverviewAdapter = adapter


    }
}