package com.test.testapplication.ui.login

import android.text.TextUtils

class LoginValidator {

    fun validateUserName(username: String?): Boolean {
        return !TextUtils.isEmpty(username)
    }

    fun validatePassword(password: String?): Boolean {
        return !TextUtils.isEmpty(password)
    }
}
