package com.test.testapplication.ui.overview

import android.view.View
import com.test.testapplication.R
import com.test.testapplication.model.Overview
import com.test.testapplication.ui.base.BaseViewModel

class OverviewViewModel : BaseViewModel() {
    var adapter: OverviewAdapter = OverviewAdapter()

    var overviewListener: OverviewListener? = null

    fun addRow(avatarResId: Int, title: String, subTitle: String) {
        var bucket = Overview(avatarResId, title, subTitle)
        adapter.addItem(bucket)
    }

    fun onImaviewBackClick(view: View) {
        overviewListener?.onBackPressed()

    }
}