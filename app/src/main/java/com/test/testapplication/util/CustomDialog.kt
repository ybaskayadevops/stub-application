package com.test.testapplication.util


import android.app.Activity
import android.app.Dialog
import android.content.Context
import android.support.v7.app.AlertDialog
import com.test.testapplication.ui.base.BaseActivity

class CustomDialog() {

    lateinit var dialog: AlertDialog
    fun showAlertDialogButton(
        context: Context?,
        title: String?,
        message: String?,
        buttonMessageStr: String?
    ) {
        // setup the alert builder
        if (context != null) {
            val builder = AlertDialog.Builder(context)
            builder.setTitle(title)
            builder.setMessage(message)
            // add a button
            builder.setPositiveButton(buttonMessageStr, null)
            // create and show the alert dialog
            dialog = builder.create()

            showDialog();
        }
    }

    fun showDialog() {
        hideDialog();
        if (dialog != null && !dialog.isShowing()) {
            dialog.show()
        }
    }

    fun hideDialog() {

        if (::dialog.isInitialized) {
            if (dialog != null && dialog.isShowing()) {
                dialog.hide()
            }
        }
    }
}