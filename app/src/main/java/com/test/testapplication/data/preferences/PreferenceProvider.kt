package com.test.testapplication.data.preferences

import android.content.Context
import android.content.SharedPreferences
import android.preference.PreferenceManager

private const val KEY_SAVED_AT = "key_saved_at"
private const val LOGIN_USERNAME = "login_username"

class PreferenceProvider(context: Context) {

    private val appContext = context.applicationContext

    private val preference: SharedPreferences
        get() = PreferenceManager.getDefaultSharedPreferences(appContext)


    fun saveUserName(username: String) {
        preference.edit().putString(
            LOGIN_USERNAME,
            username
        ).apply()
    }

    fun getUserName(): String? {
        return preference.getString(LOGIN_USERNAME, null)
    }

    fun clearUserName() {
        preference.edit().putString(
            LOGIN_USERNAME,
            null
        ).apply()
    }

    fun savelastSavedAt(savedAt: String) {
        preference.edit().putString(
            KEY_SAVED_AT,
            savedAt
        ).apply()
    }

    fun getLastSavedAt(): String? {
        return preference.getString(KEY_SAVED_AT, null)
    }


}