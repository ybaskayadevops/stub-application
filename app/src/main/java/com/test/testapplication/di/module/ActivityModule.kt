package com.test.testapplication.module

import com.test.testapplication.di.annotaton.ActivityScope
import com.test.testapplication.ui.login.viewmodel.LoginActivity
import com.test.testapplication.ui.overview.OverviewActivity
import dagger.Module
import dagger.android.ContributesAndroidInjector

@Module
interface ActivityModule {


    @ActivityScope
    @ContributesAndroidInjector()
    fun contributeLoginActivity(): LoginActivity

    @ActivityScope
    @ContributesAndroidInjector()
    fun contributeOverviewActivity(): OverviewActivity


}