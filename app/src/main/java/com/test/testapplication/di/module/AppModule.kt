package com.test.testapplication.di.module

import android.app.Application
import android.content.Context
import com.test.testapplication.di.annotaton.ApplicationContext
import com.test.testapplication.ui.overview.OverviewAdapter
import dagger.Module
import dagger.Provides
import javax.inject.Singleton


@Module()
internal object AppModule {
    @Provides
    @Singleton
    @JvmStatic
    @ApplicationContext
    fun provideContext(application: Application): Context {
        return application
    }


    @Provides
    @JvmStatic
    fun provideOverviewAAdapter(): OverviewAdapter {
        return OverviewAdapter()
    }


}