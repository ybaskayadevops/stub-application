package com.test.testapplication.di.component

import android.app.Application
import com.test.testapplication.app.TestApplication
import com.test.testapplication.di.module.AppModule
import com.test.testapplication.module.ActivityModule
import dagger.BindsInstance
import dagger.Component
import javax.inject.Singleton


@Singleton
@Component(modules = [AppModule::class, ActivityModule::class])
interface AppComponent {
    @Component.Builder
    interface Builder {
        @BindsInstance
        fun application(application: Application): Builder

        fun build(): AppComponent
    }

    fun inject(app: TestApplication)
}