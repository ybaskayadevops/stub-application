package com.test.testapplication.di.annotaton

import javax.inject.Qualifier


@Qualifier
@Retention(AnnotationRetention.RUNTIME)
annotation class ApplicationContext
