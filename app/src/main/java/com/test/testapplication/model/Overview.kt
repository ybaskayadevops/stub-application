package com.test.testapplication.model

import android.os.Parcel
import android.os.Parcelable

data class Overview(var avatarResId: Int, var title: String, var subTitle: String) : Parcelable {
    constructor(parcel: Parcel) : this(
        parcel.readInt(),
        parcel.readString(),
        parcel.readString()
    )

    override fun writeToParcel(parcel: Parcel, flags: Int) {
        parcel.writeInt(avatarResId)
        parcel.writeString(title)
        parcel.writeString(subTitle)
    }

    override fun describeContents(): Int {
        return 0
    }

    companion object CREATOR : Parcelable.Creator<Overview> {
        override fun createFromParcel(parcel: Parcel): Overview {
            return Overview(parcel)
        }

        override fun newArray(size: Int): Array<Overview?> {
            return arrayOfNulls(size)
        }
    }


}