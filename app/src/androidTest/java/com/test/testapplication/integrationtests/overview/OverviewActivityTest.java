package com.test.testapplication.integrationtests.overview;

import android.content.Intent;
import android.support.test.rule.ActivityTestRule;
import android.support.test.runner.AndroidJUnit4;

import com.test.testapplication.ui.overview.OverviewActivity;

import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;

@RunWith(AndroidJUnit4.class)
public class OverviewActivityTest {

    @Rule
    public ActivityTestRule<OverviewActivity> activityRule
            = new ActivityTestRule<>(
            OverviewActivity.class,
            true,
            false);

    @Test
    public void intent() {
        Intent intent = new Intent();
        activityRule.launchActivity(intent);
    }
}