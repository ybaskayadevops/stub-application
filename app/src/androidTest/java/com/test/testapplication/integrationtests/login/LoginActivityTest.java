package com.test.testapplication.integrationtests.login;

import android.app.Activity;
import android.app.Instrumentation;
import android.content.Context;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;
import android.support.annotation.StringRes;
import android.support.test.InstrumentationRegistry;
import android.support.test.espresso.matcher.ViewMatchers;
import android.support.test.rule.ActivityTestRule;
import android.support.test.runner.AndroidJUnit4;
import android.view.View;
import android.widget.EditText;

import com.test.testapplication.R;
import com.test.testapplication.data.preferences.PreferenceProvider;
import com.test.testapplication.model.Overview;
import com.test.testapplication.ui.login.viewmodel.LoginActivity;
import com.test.testapplication.ui.overview.OverviewActivity;

import org.hamcrest.Description;
import org.hamcrest.Matcher;
import org.hamcrest.TypeSafeMatcher;
import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;

import static android.support.test.InstrumentationRegistry.getInstrumentation;
import static android.support.test.espresso.Espresso.onView;
import static android.support.test.espresso.action.ViewActions.clearText;
import static android.support.test.espresso.action.ViewActions.click;
import static android.support.test.espresso.action.ViewActions.closeSoftKeyboard;
import static android.support.test.espresso.action.ViewActions.typeText;
import static android.support.test.espresso.assertion.ViewAssertions.doesNotExist;
import static android.support.test.espresso.assertion.ViewAssertions.matches;
import static android.support.test.espresso.intent.Intents.intended;
import static android.support.test.espresso.intent.matcher.IntentMatchers.hasComponent;
import static android.support.test.espresso.matcher.RootMatchers.isDialog;
import static android.support.test.espresso.matcher.RootMatchers.withDecorView;
import static android.support.test.espresso.matcher.ViewMatchers.isDisplayed;
import static android.support.test.espresso.matcher.ViewMatchers.withId;
import static android.support.test.espresso.matcher.ViewMatchers.withText;
import static org.hamcrest.core.IsNot.not;
import static org.junit.Assert.assertNotNull;

/**
 * Created by teerapong on 7/7/2016 AD.
 */
@RunWith(AndroidJUnit4.class)
public class LoginActivityTest {

    @Rule
    public ActivityTestRule<LoginActivity> activityTestRule =
            new ActivityTestRule<LoginActivity>(LoginActivity.class) {
                @Override
                protected void beforeActivityLaunched() {
                    clearSharedPrefs(InstrumentationRegistry.getTargetContext());
                    super.beforeActivityLaunched();
                }
            };


    private void clearSharedPrefs(Context context) {
        SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(context);
        SharedPreferences.Editor editor = prefs.edit();
        editor.clear();
        editor.commit();
    }


    @Test
    public void usernameIsEmpty() {
        onView(ViewMatchers.withId(R.id.input_username)).perform(clearText());
        onView(withId(R.id.buttonLogin)).perform(click());
        onView(withText(R.string.warning))
                .inRoot(isDialog()) // <---
                .check(matches(isDisplayed()));
    }

    @Test
    public void passwordIsEmpty() {

        onView(withId(R.id.input_username)).perform(typeText("username"), closeSoftKeyboard());
        onView(withId(R.id.input_password)).perform(clearText());
        onView(withId(R.id.buttonLogin)).perform(click());
        onView(withText(R.string.warning))
                .inRoot(isDialog()) // <---
                .check(matches(isDisplayed()));
    }


    @Test
    public void loginSuccessfully_shouldOpenOverviewActivity() {

        Instrumentation.ActivityMonitor activityMonitor = getInstrumentation().
                addMonitor(OverviewActivity.class.getName(), null, false);

        onView(withId(R.id.input_username)).perform(typeText("username"), closeSoftKeyboard());
        onView(withId(R.id.input_password)).perform(typeText("123456"), closeSoftKeyboard());
        onView(withId(R.id.buttonLogin)).perform(click());

        Activity overviewActivity = getInstrumentation().waitForMonitorWithTimeout(activityMonitor, 5000);

        assertNotNull(overviewActivity);
        overviewActivity.finish();
    }


}