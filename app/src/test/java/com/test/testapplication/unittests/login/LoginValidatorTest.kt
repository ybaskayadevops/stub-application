package com.test.testapplication.unittesting.login.model


import com.test.testapplication.ui.login.LoginValidator
import org.assertj.core.api.Assertions.assertThat
import org.junit.Test
import org.junit.runner.RunWith
import org.robolectric.RobolectricTestRunner

@RunWith(RobolectricTestRunner::class)
class LoginValidatorTest {

    val objectUnderTest = LoginValidator()

    @Test
    fun `null username is invalid`() {
        //when
        val result = objectUnderTest.validateUserName(null)
        //then
        assertThat(result).isFalse()
    }

    @Test
    fun `empty username is invalid`() {
        //when
        val result = objectUnderTest.validateUserName("")
        //then
        assertThat(result).isFalse()
    }

    @Test
    fun `not empty username is valid`() {
        //when
        val result = objectUnderTest.validateUserName("anyUserName")
        //then
        assertThat(result).isTrue()
    }

    @Test
    fun `null password is invalid`() {
        //when
        val result = objectUnderTest.validatePassword(null)
        //then
        assertThat(result).isFalse()
    }


    @Test
    fun `empty password is invalid`() {
        //when
        val result = objectUnderTest.validatePassword("")
        //then
        assertThat(result).isFalse()
    }

    @Test
    fun `not empty password is valid`() {
        //when
        val result = objectUnderTest.validatePassword("123456")
        //then
        assertThat(result).isTrue()
    }

}